# Отчёт по выполненной работе "Верхнетреугольные матрицы на шаблонах"
## Введение

**Цель данной работы** — создание программных средств, поддерживающих эффективное хранение верхнетреугольных матриц и выполнение основных операций над ними (сложение, вычитание, копирование, сравнение), а также написание тестов с помощью фреймворка [Google Test][gtest].

Для разработки использовался язык C++.

Файл .gitignore взят [отсюда][gitignore].

## Метод реализации
Существует несколько потенциальных вариантов реализации структуры данных "верхнетреугольная матрица".

Так, можно смотреть на матрицу специального вида как на обычную матрицу, используя для её хранения двумерный массив. Существует также способ "плотной упаковки данных": строки матрицы в таком случае будут храниться линейно друг за другом. Ещё один вариант — создать массив с указателями на векторы (возникает задача реализации класса "вектор").

На последний вариант можно посмотреть немного иначе: массив, по сути, является вектором в математическом смысле, а значит, на нашу матрицу специального вида можно смотреть как на вектор векторов. Чем хороша такая реализация? Операции, написанные в классе "вектор", в таком случае будут применимы не только к элементам любых типов, если мы "шаблонизируем" класс. У нас не будет необходимости писать функцию, например, сложения отдельно для векторов и для матрицы.

В данной работе как раз и реализован последний вариант хранения верхнетреугольных матриц.

Ниже представлены исходные коды с комментариями, тесты, тестовая программа, результаты работы тестов и тестового приложения

## Реализация

### Описания классов и заголовок файла (utmatrix.h)
```C++
#include <iostream>

using namespace std;

const int MAX_VECTOR_SIZE = 100000000;
const int MAX_MATRIX_SIZE = 10000;

// Шаблон вектора
template <class ValType>
class TVector
{
protected:
  ValType *pVector;
  int Size;       // размер вектора
  int StartIndex; // индекс первого элемента вектора
public:
  TVector(int s = 10, int si = 0);
  TVector(const TVector &v);                // конструктор копирования
  ~TVector();
  int GetSize()      { return Size;       } // размер вектора
  int GetStartIndex(){ return StartIndex; } // индекс первого элемента
  ValType& operator[](int pos);             // доступ
  bool operator==(const TVector &v) const;  // сравнение
  bool operator!=(const TVector &v) const;  // сравнение
  TVector& operator=(const TVector &v);     // присваивание

  // скалярные операции
  TVector  operator+(const ValType &val);   // прибавить скаляр
  TVector  operator-(const ValType &val);   // вычесть скаляр
  TVector  operator*(const ValType &val);   // умножить на скаляр

  // векторные операции
  TVector  operator+(const TVector &v);     // сложение
  TVector  operator-(const TVector &v);     // вычитание
  ValType  operator*(const TVector &v);     // скалярное произведение
};

// ... РЕАЛИЗАЦИЯ ВЕКТОРА ...

template <class ValType>
class TMatrix : public TVector<TVector<ValType> >
{
public:
  TMatrix(int s = 10);                           
  TMatrix(const TMatrix &mt);                    // копирование
  TMatrix(const TVector<TVector<ValType> > &mt); // преобразование типа
  bool operator==(const TMatrix &mt) const;      // сравнение
  bool operator!=(const TMatrix &mt) const;      // сравнение
  TMatrix& operator= (const TMatrix &mt);        // присваивание
  TMatrix  operator+ (const TMatrix &mt);        // сложение
  TMatrix  operator- (const TMatrix &mt);        // вычитание
};

// ... РЕАЛИЗАЦИЯ МАТРИЦЫ ...

```

### Реализация класса "Вектор"
#### Исключения, выбрасываемые классами "Вектор" и "Матрица"
Возникающие в результате работы программы ошибки не будем обрабатывать конкретно, а будем выбрасывать исключения со следующими кодами:
* **1** — недопустимая длина вектора;
* **2** — отрицательная величина стартового индекса вектора;
* **3** — выход за пределы вектора (запрашиваемый элемент не существует);
* **4** — размеры векторов не совпадают.

#### Конструктор по умолчанию

```C++
template <class ValType>
TVector<ValType>::TVector(int s, int si) : Size(s), StartIndex(si) {

	if (s < 0 || s > MAX_VECTOR_SIZE) {
		throw 1;
	}

	if (si < 0 || si > MAX_VECTOR_SIZE) {
		throw 2;
	}

	pVector = new ValType[Size];

}
```

#### Конструтор копирования

```C++
template <class ValType>
TVector<ValType>::TVector(const TVector<ValType> &v) {

	Size = v.Size;
	StartIndex = v.StartIndex;

	pVector = new ValType[Size];

	for (int i = 0; i < Size; i++) {
		pVector[i] = v.pVector[i];
	}

}
```

#### Деструктор

```C++
template <class ValType>
TVector<ValType>::~TVector() {

	delete[] pVector;

}
```

#### Доступ к элементам массива

```C++
template <class ValType> // доступ
ValType& TVector<ValType>::operator[](int pos) {

	if (pos < StartIndex || pos >= StartIndex + Size) {
		throw 3;
	}

	return pVector[pos - StartIndex];

}
```

#### Сравнение двух векторов

```C++
template <class ValType> // сравнение
bool TVector<ValType>::operator==(const TVector &v) const {

	bool equiv = true;

	if (Size != v.Size || StartIndex != v.StartIndex) {
		equiv = false;
	}

	if (equiv) {
		for (int i = 0; i < Size; i++) {
			if (pVector[i] != v.pVector[i]) {
				equiv = false;
			}
		}
	}
	
	return equiv;

}

template <class ValType>
bool TVector<ValType>::operator!=(const TVector &v) const {

	return !(*this == v);

}
```

#### Присваивание
```C++
template <class ValType>
TVector<ValType>& TVector<ValType>::operator=(const TVector &v) {

	if (&v != this) {

		delete[] pVector;

		Size = v.Size;
		StartIndex = v.StartIndex;

		pVector = new ValType[Size];
		
		for (int i = 0; i < Size; i++) {
			pVector[i] = v.pVector[i];
		}

	}

	return *this;

}
```

#### Прибавить и вычесть скаляр, умножить на скаляр
```C++
template <class ValType>
TVector<ValType> TVector<ValType>::operator+(const ValType &val) {

	TVector<ValType> temp(Size, StartIndex);

	for (int i = 0; i < Size; i++) {
		temp.pVector[i] = pVector[i] + val;
	}

	return temp;

}

template <class ValType>
TVector<ValType> TVector<ValType>::operator-(const ValType &val) {

	return *this + (-val);

}

template <class ValType>
TVector<ValType> TVector<ValType>::operator*(const ValType &val) {

	TVector<ValType> temp(Size, StartIndex);

	for (int i = 0; i < Size; i++) {
		temp.pVector[i] = pVector[i] * val;
	}

	return temp;

}
```

#### Сложение, вычитание, скалярное произведение
```C++
template <class ValType>
TVector<ValType> TVector<ValType>::operator+(const TVector<ValType> &v) {

	if (Size != v.Size || StartIndex != v.StartIndex) {
		throw 4;
	}

	TVector<ValType> temp(Size, StartIndex);

	for (int i = 0; i < Size; i++) {
		temp.pVector[i] = pVector[i] + v.pVector[i];
	}

	return temp;

}

template <class ValType>
TVector<ValType> TVector<ValType>::operator-(const TVector<ValType> &v) {

	if (Size != v.Size || StartIndex != v.StartIndex) {
		throw 4;
	}

	TVector<ValType> temp(Size, StartIndex);

	for (int i = 0; i < Size; i++) {
		temp.pVector[i] = pVector[i] - v.pVector[i];
	}

	return temp;

}

template <class ValType>
ValType TVector<ValType>::operator*(const TVector<ValType> &v) {

	if (Size != v.Size || StartIndex != v.StartIndex) {
		throw 4;
	}

	ValType result;

	// Установка первоначального значения
	if (Size > 0) {
		result = pVector[0] * v.pVector[0];
	}

	for (int i = 1; i < Size; i++) {
		result += (pVector[i] * v.pVector[i]);
	}

	return result;

}
```

#### Ввод и вывод
```C++
  friend istream& operator>>(istream &in, TVector &v) {

    for (int i = 0; i < v.Size; i++)
      in >> v.pVector[i];
    return in;

  }

  friend ostream& operator<<(ostream &out, const TVector &v) {

    for (int i = 0; i < v.Size; i++)
      out << v.pVector[i] << ' ';
    return out;

  }
```

### Тесты для проверки корректности реализации, написанные для фреймворка Google Test
```C++
#include "utmatrix.h"

#include <gtest.h>

TEST(TVector, can_create_vector_with_positive_length)
{
  ASSERT_NO_THROW(TVector<int> v(5));
}

TEST(TVector, cant_create_too_large_vector)
{
  ASSERT_ANY_THROW(TVector<int> v(MAX_VECTOR_SIZE + 1));
}

TEST(TVector, throws_when_create_vector_with_negative_length)
{
  ASSERT_ANY_THROW(TVector<int> v(-5));
}

TEST(TVector, throws_when_create_vector_with_negative_startindex)
{
  ASSERT_ANY_THROW(TVector<int> v(5, -2));
}

TEST(TVector, can_create_copied_vector)
{
  TVector<int> v(10);

  ASSERT_NO_THROW(TVector<int> v1(v));
}

TEST(TVector, copied_vector_is_equal_to_source_one)
{
	TVector<int> v(5);
	TVector<int> c = v;

	EXPECT_EQ(v, c);
}

TEST(TVector, copied_vector_has_its_own_memory)
{
	TVector<int> v(5);
	TVector<int> c = v;
	c[3] = 1415;

	EXPECT_NE(v, c);
}

TEST(TVector, can_get_size)
{
  TVector<int> v(4);

  EXPECT_EQ(4, v.GetSize());
}

TEST(TVector, can_get_start_index)
{
  TVector<int> v(4, 2);

  EXPECT_EQ(2, v.GetStartIndex());
}

TEST(TVector, can_set_and_get_element)
{
  TVector<int> v(4);
  v[0] = 4;

  EXPECT_EQ(4, v[0]);
}

TEST(TVector, throws_when_set_element_with_negative_index)
{
	TVector<int> v(5);

	ASSERT_ANY_THROW(v[-1] = 1);
}

TEST(TVector, throws_when_set_element_with_too_large_index)
{
	TVector<int> v(5);

	ASSERT_ANY_THROW(v[6] = 1);
}

TEST(TVector, can_assign_vector_to_itself)
{
	TVector<int> v(2);
	v[0] = 10;
	v[1] = 89;
	v = v;

	EXPECT_EQ(v[0], 10);
	EXPECT_EQ(v[1], 89);
}

TEST(TVector, can_assign_vectors_of_equal_size)
{
	TVector<int> v1(2), v2(2);
	v1[0] = 5; v1[1] = 6;
	v2[0] = 7; v2[1] = 8;
	v2 = v1;

	EXPECT_EQ(v2[0], 5);
	EXPECT_EQ(v2[1], 6);
}

TEST(TVector, assign_operator_change_vector_size)
{
	TVector<int> v1(5), v2(15);

	v2 = v1;
	EXPECT_EQ(v2.GetSize(), 5);
}

TEST(TVector, can_assign_vectors_of_different_size)
{
	TVector<int> v1(2), v2(3);
	v1[0] = 5; v1[1] = 6;
	v2 = v1;

	EXPECT_EQ(v2[0], 5);
	EXPECT_EQ(v2[1], 6);
}

TEST(TVector, compare_equal_vectors_return_true)
{
	TVector<int> v(5);
	TVector<int> c = v;

	EXPECT_TRUE(v == c);
}

TEST(TVector, compare_vector_with_itself_return_true)
{
	TVector<int> v(5);

	EXPECT_TRUE(v == v);
}

TEST(TVector, vectors_with_different_size_are_not_equal)
{
	TVector<int> v1(2), v2(3);

	EXPECT_FALSE(v1 == v2);
}

TEST(TVector, can_add_scalar_to_vector)
{
	TVector<int> v1(2), v2(2);
	v1[0] = 5;
	v1[1] = 6;
	v2 = v1 + 7;

	EXPECT_EQ(v2[0], 12);
	EXPECT_EQ(v2[1], 13);
}

TEST(TVector, can_subtract_scalar_from_vector)
{
	TVector<int> v1(2), v2(2);
	v1[0] = 5;
	v1[1] = 6;
	v2 = v1 - 7;

	EXPECT_EQ(v2[0], -2);
	EXPECT_EQ(v2[1], -1);
}

TEST(TVector, can_multiply_scalar_by_vector)
{
	TVector<int> v1(2), v2(2);
	v1[0] = 5;
	v1[1] = 6;
	v2 = v1 * 2;

	EXPECT_EQ(v2[0], 10);
	EXPECT_EQ(v2[1], 12);
}

TEST(TVector, can_add_vectors_with_equal_size)
{
	TVector<int> v1(2), v2(2), v3(2);
	v1[0] = 5; v1[1] = 6;
	v2[0] = 7; v2[1] = 8;
	v3 = v2 + v1;

	EXPECT_EQ(v3[0], 12);
	EXPECT_EQ(v3[1], 14);
}

TEST(TVector, cant_add_vectors_with_not_equal_size)
{
	TVector<int> v1(2), v2(3);
	v1[0] = 5; v1[1] = 6;
	v2[0] = 7; v2[1] = 8; v2[2] = 9;

	EXPECT_ANY_THROW(v2 + v1);
}

TEST(TVector, can_subtract_vectors_with_equal_size)
{
	TVector<int> v1(2), v2(2), v3(2);
	v1[0] = 5; v1[1] = 6;
	v2[0] = 7; v2[1] = 8;
	v3 = v2 - v1;

	EXPECT_EQ(v3[0], 2);
	EXPECT_EQ(v3[1], 2);
}

TEST(TVector, cant_subtract_vectors_with_not_equal_size)
{
	TVector<int> v1(2), v2(3);
	v1[0] = 5; v1[1] = 6;
	v2[0] = 7; v2[1] = 8; v2[2] = 9;

	EXPECT_ANY_THROW(v2 - v1);
}

TEST(TVector, can_multiply_vectors_with_equal_size)
{
	TVector<int> v1(2), v2(2);
	v1[0] = 1; v1[1] = 2;
	v2[0] = 3; v2[1] = 4;
	int result = v2 * v1;

	EXPECT_EQ(result, 11);
}

TEST(TVector, cant_multiply_vectors_with_not_equal_size)
{
	TVector<int> v1(2), v2(3);
	v1[0] = 1; v1[1] = 2;
	v2[0] = 3; v2[1] = 4; v2[2] = 5;

	EXPECT_ANY_THROW(v2 * v1);
}


```

### Реализация класса "Верхнетреугольная матрица"
#### Исключение, выбрасываемое данным классом
* **5** — слишком большой размер матрицы.

#### Конструктор по умолчанию
```C++
template <class ValType>
TMatrix<ValType>::TMatrix(int s): TVector<TVector<ValType> >(s) {

	if (s > MAX_MATRIX_SIZE) {
		throw 5;
	}

	for (int i = 0; i < s; i++) {
		pVector[i] = TVector<ValType>(s - i, i);
	}


}
```

#### Конструктор копирования и преобразования типа
```C++
template <class ValType>
TMatrix<ValType>::TMatrix(const TMatrix<ValType> &mt):
  TVector<TVector<ValType> >(mt) {}

template <class ValType>
TMatrix<ValType>::TMatrix(const TVector<TVector<ValType> > &mt):
  TVector<TVector<ValType> >(mt) {}
```

#### Сравнение
```C++
template <class ValType>
bool TMatrix<ValType>::operator==(const TMatrix<ValType> &mt) const {

	return TVector<TVector<ValType> >::operator==(mt);

}

template <class ValType>
bool TMatrix<ValType>::operator!=(const TMatrix<ValType> &mt) const {

	return !( TVector<TVector<ValType> >::operator==(mt) );

}
```

#### Присванивание
```C++
template <class ValType>
TMatrix<ValType>& TMatrix<ValType>::operator=(const TMatrix<ValType> &mt) {

	TVector<TVector<ValType> >::operator=(mt);
	return *this;

}
```

#### Сложение и вычитание
```C++
template <class ValType>
TMatrix<ValType> TMatrix<ValType>::operator+(const TMatrix<ValType> &mt) {

	return TVector<TVector<ValType> >::operator+(mt);

} 

template <class ValType>
TMatrix<ValType> TMatrix<ValType>::operator-(const TMatrix<ValType> &mt) {

	return TVector<TVector<ValType> >::operator-(mt);

}
```

#### Ввод и вывод
```C++
friend istream& operator>>(istream &in, TMatrix &mt) {

    for (int i = 0; i < mt.Size; i++)
      in >> mt.pVector[i];
    return in;

  }

  friend ostream& operator<<( ostream &out, const TMatrix &mt) {

    for (int i = 0; i < mt.Size; i++)
      out << mt.pVector[i] << endl;
    return out;

  }
```

### Тесты Google Test
```C++
#include "utmatrix.h"

#include <gtest.h>

TEST(TMatrix, can_create_matrix_with_positive_length)
{
  ASSERT_NO_THROW(TMatrix<int> m(5));
}

TEST(TMatrix, cant_create_too_large_matrix)
{
  ASSERT_ANY_THROW(TMatrix<int> m(MAX_MATRIX_SIZE + 1));
}

TEST(TMatrix, throws_when_create_matrix_with_negative_length)
{
  ASSERT_ANY_THROW(TMatrix<int> m(-5));
}

TEST(TMatrix, can_create_copied_matrix)
{
  TMatrix<int> m(5);

  ASSERT_NO_THROW(TMatrix<int> m1(m));
}

TEST(TMatrix, copied_matrix_is_equal_to_source_one)
{
	TMatrix<int> m(5);
	TMatrix<int> c = m;

	EXPECT_EQ(m, c);
}

TEST(TMatrix, copied_matrix_has_its_own_memory)
{
	TMatrix<int> m(5);
	TMatrix<int> c = m;
	c[3][3] = 3;

	EXPECT_NE(m, c);
}

TEST(TMatrix, can_get_size)
{
	TMatrix<int> m(4);

	EXPECT_EQ(m.GetSize(), 4);
}

TEST(TMatrix, can_set_and_get_element)
{
	TMatrix<int> m(5);
	m[3][3] = 3;
	
	EXPECT_EQ(m[3][3], 3);
}

TEST(TMatrix, throws_when_set_element_with_negative_index)
{
	TMatrix<int> m(5);

	ASSERT_ANY_THROW(m[-1][-1] = 1);
}

TEST(TMatrix, throws_when_set_element_with_too_large_index)
{
	TMatrix<int> m(5);

	ASSERT_ANY_THROW(m[5][5] = 1);
}

TEST(TMatrix, can_assign_matrix_to_itself)
{
	TMatrix<int> m(2);
	m[0][0] = 0;
	m[0][1] = 1;
	m[1][1] = 11;
	m = m;

	EXPECT_EQ(m[0][0], 0);
	EXPECT_EQ(m[0][1], 1);
	EXPECT_EQ(m[1][1], 11);
}

TEST(TMatrix, can_assign_matrices_of_equal_size)
{
	TMatrix<int> m1(2), m2(2);
	m1[0][0] = 0;
	m1[0][1] = 1;
	m1[1][1] = 11;
	m2 = m1;

	EXPECT_EQ(m2[0][0], 0);
	EXPECT_EQ(m2[0][1], 1);
	EXPECT_EQ(m2[1][1], 11);
}

TEST(TMatrix, assign_operator_change_matrix_size)
{
	TMatrix<int> m1(2), m2(3);
	m2 = m1;

	EXPECT_EQ(m2.GetSize(), 2);
}

TEST(TMatrix, can_assign_matrices_of_different_size)
{
	TMatrix<int> m1(2), m2(3);
	m1[0][0] = 0;
	m1[0][1] = 1;
	m1[1][1] = 11;
	m2 = m1;

	EXPECT_EQ(m2[0][0], 0);
	EXPECT_EQ(m2[0][1], 1);
	EXPECT_EQ(m2[1][1], 11);
}

TEST(TMatrix, compare_equal_matrices_return_true)
{
	TMatrix<int> m(2);
	TMatrix<int> c = m;

	EXPECT_TRUE(c == m);
}

TEST(TMatrix, compare_matrix_with_itself_return_true)
{
	TMatrix<int> m(2);

	EXPECT_TRUE(m == m);
}

TEST(TMatrix, matrices_with_different_size_are_not_equal)
{
	TMatrix<int> m1(2), m2(3);

	EXPECT_FALSE(m1 == m2);
}

TEST(TMatrix, can_add_matrices_with_equal_size)
{
	TMatrix<int> m1(2), m2(2), m3(2);
	m1[0][0] = 0; m1[0][1] = 1; m1[1][1] = 11;
	m2[0][0] = 20; m2[0][1] = 21; m2[1][1] = 211;
	m3 = m2 + m1;

	EXPECT_EQ(m3[0][0], 20);
	EXPECT_EQ(m3[0][1], 22);
	EXPECT_EQ(m3[1][1], 222);
}

TEST(TMatrix, cant_add_matrices_with_not_equal_size)
{
	TMatrix<int> m1(1), m2(2);
	m1[0][0] = 1;
	m2[0][0] = 0; m2[0][1] = 1; m2[1][1] = 11;

	EXPECT_ANY_THROW(m2 + m1);
}

TEST(TMatrix, can_subtract_matrices_with_equal_size)
{
	TMatrix<int> m1(2), m2(2), m3(2);
	m1[0][0] = 0; m1[0][1] = 1; m1[1][1] = 11;
	m2[0][0] = 11; m2[0][1] = 11; m2[1][1] = 11;
	m3 = m2 - m1;

	EXPECT_EQ(m3[0][0], 11);
	EXPECT_EQ(m3[0][1], 10);
	EXPECT_EQ(m3[1][1], 0);
}

TEST(TMatrix, cant_subtract_matrixes_with_not_equal_size)
{
	TMatrix<int> m1(1), m2(2);
	m1[0][0] = 1;
	m2[0][0] = 0; m2[0][1] = 1; m2[1][1] = 11;

	EXPECT_ANY_THROW(m2 - m1);
}
```

## Тестовое приложение


```C++
#include <iostream>
#include "utmatrix.h"

void main()
{
  TMatrix<int> a(5), a_copy(5), b(5), c(5), d(5), e(5);
  int i, j;

  setlocale(LC_ALL, "Russian");
  cout << "Тестирование программ поддержки представления треугольных матриц"
    << endl;
  for (i = 0; i < 5; i++)
    for (j = i; j < 5; j++ )
    {
      a[i][j] =  i * 10 + j;
	  a_copy[i][j] = i * 10 + j;
      b[i][j] = (i * 10 + j) * 100;
    }
  c = a + b;
  d = b - a;
  cout << "Matrix a = " << endl << a << endl;
  cout << "Matrix b = " << endl << b << endl;
  cout << "Matrix c = a + b" << endl << c << endl;
  cout << "Matrix d = b - a" << endl << d << endl;
  cout << "a == c? " << (a == c) << endl;
  cout << "a == a_copy? " << (a == a_copy) << endl;
}
```

## Приложения

#### Рис. 1. Успешное выполнение тестов для классов "Вектор" и "Матрица"

![Успешное выполнение тестов для классов "Вектор" и "Матрица"](1.png)

#### Рис. 2. Пример работы тестового приложения

![Рис. 2. Пример работы тестового приложения](2.png)

## Выводы
Из множества вариантов решения задачи создания структуры данных для хранения верхнетреугольных матриц именно реализованный хорошо "стыкуется" со средствами ООП в C++. Реализация классов получилась вполне естественной и удобной для использования. Google Test, как всегда, помог "отловить" несколько незамеченных сразу ошибок.

[gitignore]: 	https://github.com/github/gitignore
[gtest]:      	https://github.com/google/googletest

