// ����, ���, ���� "������ ����������������-2", �++, ���
//
// sample_matrix.cpp - Copyright (c) ������� �.�. 07.05.2001
//   ������������ ��� Microsoft Visual Studio 2008 �������� �.�. (20.04.2015)
//
// ������������ ����������������� �������

#include <iostream>
#include "utmatrix.h"
//---------------------------------------------------------------------------

void main()
{
	const int val = 5;

	TMatrix<int> a(val), b(val), c(val);
	int i, j;

	setlocale(LC_ALL, "Russian");

	cout << "������������ �������� ��������� ������������� ����������� ������"
		<< endl;
	for (i = 0; i < val; i++)
		for (j = i; j < val; j++)
		{
			a[i][j] = i * 10 + j;
			b[i][j] = 2 * i + j * 20;
		}
	
	c = (b-a);
	cout << "Matrix a = " << endl << a << endl;
	cout << "Matrix b = " << endl << b << endl;
	cout << "c=(a-b)\n\nComparison of a and c:\n";
	
	if (a == c)
		cout << "a=c\n\n";
	else
		cout << "a!=c\n\n";

	TMatrix<char> CH(val);
	for (i = 0; i < val; i++)
		for (j = i; j < val; j++)
			CH[i][j] = 65 + i;

	cout << "Matrix of chars = " << endl << CH << endl;
	

}
//---------------------------------------------------------------------------
