#include <string>
#include <iostream>
#include "utmatrix.h"
#include "gtest.h"
#include <map>
#include "test_program_function.h"

using namespace std;

int main()
{
	string console, command, arg[5] = { "", "", "", "", "" };
	map<string, TMatrix<int>> matrix;
	setlocale(LC_ALL, "Russian");
	cout << "���������������� ��������� ������������ ��������� ����������������� �������." << endl;
	cout << "��� ��������� ���������� � ������������ ������� help." << endl;
	do
	{
		command.clear();
		console.clear();
		getline(cin, console);
		if (console.size())
		{
			int i, f = 0;
			for (i = 0; i < console.size() && console.at(i) != (' '); i++)
				command.push_back(console.at(i));
			i++;
			for (; i < console.size() && f < 5; i++)
			{
				if (console.at(i) != ' ')
					arg[f].push_back(console.at(i));
				else f++;
			}
			try
			{
				if (command_list(command)) throw ("�������� " + command + " �� ����������");
				if (f == 5) throw "�������� ���������� ����������";
				if (command == "help") help();
				if (command == "create") create(arg, matrix);
				if (command == "delete") del(arg, matrix);
				if (command == "info") info(arg, matrix);
				if (command == "add") add(arg, matrix);
				if (command == "subtract") subtract(arg, matrix);
				if (command == "push") push(arg, matrix);
				if (command == "equal") equal(arg, matrix);
				if (command == "assigned") assigned(arg, matrix);
			}
			catch (string err) { cout << err << endl; }
			catch (char *err) { cout << err << endl; }
		}
		for (int i = 0; i < 5; i++)
			arg[i] = "";
	} while (command != "exit");
	return 0;
}
